var express = require('express')
const app = express()
var http = require('http').Server(app)
var io = require('socket.io')(http)

var bodyParser = require('body-parser')
var session = require('express-session')
app.set('view engine', 'ejs')
app.use(express.static('public'))
app.use(bodyParser.urlencoded({extended: false}))
app.use(bodyParser.json())

app.use(session({
    secret: process.env.SESS_SECRET || "sess_secret",
    resave: false,
    saveUninitialized: true,
    cookie: {secure: false}
}))

const port = process.env.PORT || 3000
const password = "test"


app.get("/", (req, res) => {
    if (req.session.logged !== true) {
        //Not connected
        res.render('pages/login', {pageName: "Login"})
    } else {
        //Connected
        res.render('pages/main', {pageName: "Chat"})
    }
})

app.post('/login', (req, res) => {
    if (password === req.body.password) {
        req.session.logged = true;
        res.redirect('/')
    } else {
        res.send("Bad login")
    }
})

http.listen(port, () => {
    console.log(`Server open on port ${port}`)
})